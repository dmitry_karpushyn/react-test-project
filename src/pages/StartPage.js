import React from 'react';
import _ from 'lodash';
import TodoCreate from '../components/todo-create';
import TodoList from '../components/todo-list';


const todos = [
  {
    task: "testing 1",
    isCompleted: false
  },
  {
    task: "testing 2",
    isCompleted: true
  }
];

export default class StartPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: todos,
      selectedFilter: "All"
    };
  }

  onFilterChange(selectedFilter) {
    this.setState({ selectedFilter });
  }

  filterTodoItems = (item) => {
    const { selectedFilter } = this.state;
    if (selectedFilter === "All") {
      return item;
    }
    if (selectedFilter === "Undone") {
      return !item.isCompleted;
    } else {
      return item.isCompleted;
    }
  }

  createTask = (task) => {
    if (task !== "") {
      this.state.todos.push({
        task,
        id: Math.random(),
        isCompleted: false
      });
      this.setState({ todos: this.state.todos });
    } else {
      alert("Enter correct value");
    }
  }

  deleteTask = (taskToDelete) => {
    _.remove(this.state.todos, todo => todo.id === taskToDelete);
    this.setState({ todos: this.state.todos });
  };

  saveTask = (oldTask, newTask) => {
    const foundTodo = _.find(this.state.todos, todo => todo.task === oldTask);
    foundTodo.task = newTask;
    this.setState({ todos: this.state.todos });
  };

  toggleTask = (task) => {
    const foundTodo = _.find(this.state.todos, todo => todo.task === task);
    foundTodo.isCompleted = !foundTodo.isCompleted;
    this.setState({ todos: this.state.todos });
  };

  render() {
    return (
      <div>
        <div className="d-flex align-items-center justify-content-between pr-3">
          <h1>TASK MANAGER V4.0</h1>
          <div>
            <button className={`btn mr-2 ${this.state.selectedFilter === "All" ? "btn-primary" : "btn-default"}`}
                    id="allbtn"
                    onClick={() => this.onFilterChange("All")}
            >
              All
            </button>
            <button className={`btn mr-2 ${this.state.selectedFilter === "Done" ? "btn-primary" : "btn-default"}`}
                    id="donebtn"
                    onClick={() => this.onFilterChange("Done")}
            >
              Done
            </button>
            <button className={`btn mr-2 ${this.state.selectedFilter === "Undone" ? "btn-primary" : "btn-default"}`}
                    id="undonebtn"
                    onClick={() => this.onFilterChange("Undone")}
            >
              Undone
            </button>
          </div>
        </div>
        <div className="td-list-con">
          <TodoCreate
            todos={this.state.todos}
            createTask={this.createTask}
          />
          <div className="row ">
            <div className="col-7"><h2>Task</h2></div>
            <div className="col-2 text-right"><h2>Status</h2></div>
            <div className="col-3 text-right"><h2>Actions</h2></div>
          </div>
          <TodoList
            todos={this.state.todos.filter(this.filterTodoItems)}
            saveTask={this.saveTask}
            deleteTask={this.deleteTask}
            toggleTask={this.toggleTask}
          />
        </div>
      </div>
    );
  }
}
