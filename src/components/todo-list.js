import React from 'react';
import _ from 'lodash';

import TodoListItem from './todo-list-item';

export default class TodoList extends React.Component {
  renderTodoItems() {
  // const props = _.omit(this.props, 'todos');
    const props =this.props;
  // console.log(props);
    return _.map(this.props.todos, (todo, index) => <TodoListItem key={index} {...todo} {...props} />);
  }

  render() {
    return (
      <div className="list form-horizontal">
        {this.renderTodoItems()}
      </div>
    );
  }
}
