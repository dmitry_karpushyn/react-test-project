import React from 'react';
import _ from 'lodash';

export default class TodoCreate extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.refs.createInput.focus();
  }

  handleCreate = (event) => {
    event.preventDefault();
    const createInput = this.refs.createInput;
    const task = createInput.value;
    this.props.createTask(task);
    this.refs.createInput.value = "";
  };

  render() {
    return (
      <form className="create form-horizontal" onSubmit={this.handleCreate}>
        <div className="form-group row">
          <div className="col-md-10">
            <input className="form-control" type="text" ref="createInput" placeholder="enter task here"/>
          </div>
          <div className="col-md-2 text-right">
            <button type="submit" className="btn btn-default">Create</button>
          </div>
        </div>
      </form>
    );
  }
}
